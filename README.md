# README #

### What is this repository for? ###

* Canvas2 is a starter framework for RAW HTML project. it consists of all the files needed to quick start the UI development process.
* Canvas2 uses SASS to manage all the styles in the app. it comes pre built with most of the useful classes and mixins. its has taken ideas from bootstrap and foundation. so big thanks for those developers.
* 1.0.0

### How do I get set up? ###

* Clone the Repo.
* Go in to the Canvas2 folder and remove the git file. in mac the command for this is **rm -rf .git**
* Install the Dependancies **bower install**
* Thats all. change the folder name to your linking and your good to go.